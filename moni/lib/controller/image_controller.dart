String getImage(String topic) {
  switch (topic) {
    case 'Calculus':
      return 'assets/images/tutor.jpeg';
    case 'Physics':
      return 'assets/images/physics.jpg';
    case 'Dancing':
      return 'assets/images/dancing.jpeg';
    case 'Fitness':
      return 'assets/images/fitness.jpg';
    default:
      return 'assets/images/stock.jpeg';
  }
}
