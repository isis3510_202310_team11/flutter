import 'package:flutter/material.dart';
import 'package:moni/styles.dart';
import 'package:moni/controller/schedule_tutoring_controller.dart';
import 'package:moni/views/scheduleTutoring/schedule_tutoring_form.dart';
import '../../controller/image_controller.dart';

import '../../controller/navigation_controller.dart';
import '../../models/tutoring.dart';

class ScheduleTutoring extends StatefulWidget {
  final Tutoring tutoring;

  const ScheduleTutoring({
    Key? key,
    required this.tutoring,
  }) : super(key: key);

  @override
  _ScheduleTutoring createState() => _ScheduleTutoring();
}

class _ScheduleTutoring extends State<ScheduleTutoring> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        color: lightGrey,
      ),
      child: ListView(
        children: [
          Row(
            children: [
              Container(
                  alignment: Alignment.center,
                  margin: const EdgeInsets.fromLTRB(0, 30, 0, 40),
                  child: MaterialButton(
                    onPressed: () {
                      sessionClearForm();
                      Navigator.pop(context);
                    },
                    child: Text(
                      'Back',
                      style: rubik16lightBlue,
                    ),
                  )),
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.fromLTRB(0, 30, 0, 40),
                child: Text(
                  'Book a tutoring',
                  style: topViewTitle,
                ),
              ),
            ],
          ),
          Container(
            width: MediaQuery.of(context).size.width / 1.08,
            height: MediaQuery.of(context).size.height / 3.5,
            margin: const EdgeInsets.fromLTRB(24, 0, 24, 20),
            padding: const EdgeInsets.fromLTRB(24, 5, 24, 0),
            decoration: BoxDecoration(
                color: silverSand,
                borderRadius: BorderRadius.circular(5),
                border: Border.all(width: 0.5, color: silverSand)),
            child: Column(children: [
              Container(
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.asset(
                    getImage(widget.tutoring.topic),
                    height: MediaQuery.of(context).size.height * 0.13,
                    width: MediaQuery.of(context).size.height * 0.13,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Text(
                  widget.tutoring.title,
                  style: rubik16black,
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Text(
                  widget.tutoring.description,
                  style: rubik14black,
                ),
              ),
              Padding(
              padding: EdgeInsets.only(top: 1.0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                backgroundColor: darkBlue, // Set the background color to blue
                foregroundColor: Colors.white,// Set the text color to white
                minimumSize: Size(88, 30), 
              ),
              onPressed: () {
                //functionality
                seeReviewTap(context, widget.tutoring);
              },
              child: Padding(
                padding: EdgeInsets.all(8.0), 
                child: Text(
                  'Check Reviews',
                  style: TextStyle(fontSize: 12), 
                ),
              ),
            ),
              ),
            ]),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: const EdgeInsets.fromLTRB(25, 0, 0, 20),
              child: Text(
                'Commentaries for tutor',
                style: formTitle,
              ),
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width / 1.08,
              height: MediaQuery.of(context).size.height / 16,
              margin: const EdgeInsets.fromLTRB(24, 0, 24, 20),
              padding: const EdgeInsets.fromLTRB(24, 5, 24, 0),
              decoration: BoxDecoration(
                  color: formGrey,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(width: 0.5, color: darkGrey)),
              child: TextFormField(
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "I'd like to learn about..."),
              )),
          ScheduleTutoringForm(
            tutoringPrice: widget.tutoring.price,
            tutoringId: widget.tutoring.id!,
          ),
          MaterialButton(
              onPressed: () {
                reviewTap(context, widget.tutoring);
              },
            child: Container(
                margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                width: MediaQuery.of(context).size.width / 1.08,
                decoration: BoxDecoration(
                    color: lightBlue,
                    borderRadius: BorderRadius.circular(5),
                    boxShadow: shadowInputFields),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Review Tutoring',
                      style: rubik16white,
                    ),
                  ],
                )),
            ),
        ],
      ),
    ));
  }
}
