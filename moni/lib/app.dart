import 'package:flutter/material.dart';

import 'controller/navigation_controller.dart';


import 'package:firebase_analytics/firebase_analytics.dart';

class App extends StatefulWidget {
  
  const App({Key? key}) : super(key: key);

  static FirebaseAnalytics analytics = FirebaseAnalytics.instance;
  static FirebaseAnalyticsObserver observer =
  FirebaseAnalyticsObserver(analytics: analytics);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Moni',
      onGenerateRoute: routes(),
      theme: _theme(),
    );
  }

  ThemeData _theme() {
    return ThemeData(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      hoverColor: Colors.transparent,
    );
  }
}
