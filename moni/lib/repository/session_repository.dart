import 'package:moni/adapter/session_adapter.dart';
import 'package:moni/dao/reviews_dao.dart';

import '../DAO/session_dao.dart';
import '../models/reviewNoInternet.dart';
import '../models/sessionNoInternet.dart';
import '../models/session.dart';
import 'dart:async';

final sessionDao = SessionDao(); // Instantiate your DAO class
final reviewDao = ReviewDao();

Future<void> bookSession(Session session, bool connection) async {
  if (connection == true) {
    // No internet connection
  } else {
    // Internet connection available
    await bookSessionFB(session);
  }
}

Future<void> saveSessionNoInternet(
    SessionNoInternet sessionNI, bool connection) async {
  // No internet connection
  await sessionDao.insert(sessionNI);
}

Future<void> saveReviewNoInternet(
    ReviewNoInternet reviewNI, bool connection) async {
  // No internet connection
  await reviewDao.insert(reviewNI);
}

Future<Map<String, List<Session>>> getSessionsForUser() async {
  return getSessionsForUserFB();
}

Future<List<Session>> getSessionsForUserByDate(
    String? email, DateTime date) async {
  return getSessionsForUserByDateFB(email, date);
}
