import 'package:flutter/material.dart';
import 'package:moni/views/authentication/auth_form.dart';


class authWidget extends StatefulWidget {
  final bool mobile;
  const authWidget({Key? key, required this.mobile}) : super(key: key);

  @override
  _authWidgetState createState() => _authWidgetState();
}

// ignore: camel_case_types
class _authWidgetState extends State<authWidget> {
  var visibilityPW = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: AuthForm()));
  }
}
