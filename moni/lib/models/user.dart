class User {
  final String email;
  final String name;
  final String interes1;
  final String interes2;
  final String profilePicture;

  User(
      this.email, this.name, this.interes1, this.interes2, this.profilePicture);

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
        json["email"],
        json["name"] ?? '',
        json["interes1"],
        json["interes2"],
        json["profilePicture"] == null || json["profilePicture"] == ''
            ? 'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg'
            : json["profilePicture"]);
  }

  Map<String, dynamic> toJson() => {
        'email': email,
        'name': name,
        'interes1': interes1,
        'interes2': interes2,
        'profilePicture': profilePicture
      };
}
