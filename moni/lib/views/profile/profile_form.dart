import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:moni/controller/profile_picture_controller.dart';

import '../../network/network_connectivity.dart';
import '../../styles.dart';

class ProfileForm extends StatefulWidget {
  const ProfileForm({Key? key}) : super(key: key);

  @override
  _ProfileFormState createState() => _ProfileFormState();
}

class _ProfileFormState extends State<ProfileForm> {
  late StreamSubscription<dynamic> stream;

  Map _source = {ConnectivityResult.none: false};
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;

  @override
  void initState() {
    super.initState();

    _networkConnectivity.initialise();
    stream = _networkConnectivity.myStream.listen((source) {
      setState(() => _source = source);
    });
  }

  final _formKey = GlobalKey<FormState>();
  bool _validate = false;

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                margin: const EdgeInsets.fromLTRB(25, 20, 0, 20),
                child: Text(
                  'New profile picture',
                  style: formTitle,
                ),
              ),
            ),
            Container(
                width: MediaQuery.of(context).size.width / 1.08,
                margin: const EdgeInsets.fromLTRB(24, 0, 24, 20),
                padding: const EdgeInsets.fromLTRB(24, 5, 24, 0),
                decoration: BoxDecoration(
                    color: formGrey,
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(width: 0.5, color: darkGrey)),
                child: TextFormField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter an image URL';
                    }
                    return null;
                  },
                  controller: urlField,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText:
                          'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_2x3.jpg'),
                )),
            MaterialButton(
              onPressed: () async => {
                if (_formKey.currentState!.validate())
                  {
                    // If the form is valid, display a snackbar. In the real world,
                    // you'd often call a server or save the information in a database.
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Changing picture...')),
                    ),

                    if ((_source.keys.toList()[0] != ConnectivityResult.none &&
                        !_source.values.toList()[0]))
                      {
                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                            content: Text(
                                'There is no internet connection, the tutoring can not be created now, please try again when the connection is re-established.'))),
                      },
                    changeProfilePictureHandler(
                      context,
                    )
                  }
              },
              child: !(_source.keys.toList()[0] != ConnectivityResult.none &&
                      !_source.values.toList()[0])
                  ? Container(
                      margin: const EdgeInsets.fromLTRB(0, 14, 0, 35),
                      padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                      width: MediaQuery.of(context).size.width / 1.08,
                      decoration: BoxDecoration(
                          color: darkBlue,
                          borderRadius: BorderRadius.circular(5),
                          boxShadow: shadowInputFields),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Change',
                            style: rubik16white,
                          ),
                        ],
                      ))
                  : Container(
                      margin: const EdgeInsets.fromLTRB(0, 14, 0, 35),
                      padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                      width: MediaQuery.of(context).size.width / 1.08,
                      decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(5),
                          boxShadow: shadowInputFields),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            "Can't connect to the internet",
                            style: rubik16white,
                          ),
                        ],
                      )),
            )
          ],
        ));
  }
}
