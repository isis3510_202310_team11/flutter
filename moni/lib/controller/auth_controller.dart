import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:moni/adapter/auth_adapter.dart';

import 'navigation_controller.dart';

final TextEditingController emailField = TextEditingController();
final TextEditingController passwordField = TextEditingController();

void loginHandler(BuildContext context) async {
  bool shouldNavigate = await signInFB(emailField.text, passwordField.text);
  emailField.clear();
  passwordField.clear();
  if (shouldNavigate) {
    loginTap(context);
  } else {
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(content: Text('Incorrect email or password')),
    );
  }
}

void logoutHandler(BuildContext context) async {
  await FirebaseAuth.instance.signOut();
  Navigator.pushNamedAndRemoveUntil(
      context, authenticationRoute, (route) => false);
}
