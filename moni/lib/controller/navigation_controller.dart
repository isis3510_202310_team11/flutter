import 'package:flutter/material.dart';
import 'package:moni/models/tutoring.dart';
import 'package:moni/views/authentication/authentication.dart';
import 'package:moni/views/market/market.dart';
import 'package:moni/views/profile/profile.dart';
import 'package:moni/views/register/register.dart';
import 'package:moni/views/scheduleTutoring/see_reviews.dart';
import 'package:moni/views/search/search.dart';

import '../repository/auth_repository.dart';
import '../views/addTutoring/add_tutoring.dart';
import '../views/scheduleTutoring/review_tutoring.dart';
import '../views/scheduleTutoring/schedule_tutoring.dart';
import '../views/calendar/calendar.dart';

import 'package:moni/app.dart';

const registerRoute = '/register';
const authenticationRoute = '/authentication';
const homeRoute = '/';
const profileRoute = '/profile';
const searchRoute = '/search';
const addTutoringRoute = '/tutoring';
const calendarRoute = '/calendar';
const scheduleTutoringRoute = '/schedule_tutoring';



void registerTap(BuildContext context) {
  Navigator.pushNamed(context, registerRoute);
}


void loginTap(BuildContext context) {
  Navigator.pushNamed(context, homeRoute);
}

void homeTap(BuildContext context) {
  Navigator.pushNamed(context, homeRoute);
}

void _scheduleTutoringExplorer() {
    App.analytics.logEvent(name: 'ScheduleSessionExplorer');
  }

void scheduleTap(BuildContext context, Tutoring tutoring) {
  Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ScheduleTutoring(
                tutoring: tutoring,
              )));
  _scheduleTutoringExplorer();
}

void reviewTap(BuildContext context, Tutoring tutoring) {
  Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ReviewTutoring(
                tutoring: tutoring,
              )));
  _scheduleTutoringExplorer();
}

void seeReviewTap(BuildContext context, Tutoring tutoring) {
  Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => SeeReview(
                tutoring: tutoring,
              )));
  _scheduleTutoringExplorer();
}

RouteFactory routes() {
  return (settings) {
    final Map<String, dynamic>? arguments =
        settings.arguments as Map<String, dynamic>?;
    Widget screen;
    switch (settings.name) {
      case authenticationRoute:
        screen = Authentication();
        break;
      case registerRoute:
        screen = Register();
        break;
      case homeRoute:
        screen = isLoggedIn() ? Home() : Authentication();
        break;
      case profileRoute:
        screen = isLoggedIn() ? Profile() : Authentication();
        break;
      case addTutoringRoute:
        screen = isLoggedIn() ? AddTutoring() : Authentication();
        break;
      case calendarRoute:
        screen = isLoggedIn() ? Calendar() : Authentication();
        break;
      case searchRoute:
        screen = isLoggedIn() ? Search() : Authentication();
        break;
      default:
        return null;
    }
    return MaterialPageRoute(builder: (BuildContext context) => screen);
  };
}

void onItemTapped(BuildContext context, int index) {
  switch (index) {
    case 0:
      Navigator.pushNamed(context, homeRoute);
      break;
    case 1:
      Navigator.pushNamed(context, searchRoute);
      break;
    case 2:
      Navigator.pushNamed(context, addTutoringRoute);
      break;
    case 3:
      Navigator.pushNamed(context, calendarRoute);
      break;
    case 4:
      Navigator.pushNamed(context, profileRoute);
      break;
    default:
  }
}
