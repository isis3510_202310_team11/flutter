import 'package:firebase_auth/firebase_auth.dart' as FirebaseAuth;
import 'package:flutter/material.dart';
import 'package:moni/controller/navigation_controller.dart';

import '../repository/user_repository.dart';

final TextEditingController urlField = TextEditingController();

Future<void> changeProfilePictureHandler(BuildContext context) async {
  if (urlField.text.isEmpty) return;
  await changeProfilePicture(
      urlField.text, FirebaseAuth.FirebaseAuth.instance.currentUser?.email);
  urlField.clear();
  Navigator.pushNamed(context, profileRoute);
}
