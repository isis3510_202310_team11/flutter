
import 'package:flutter/material.dart';
import 'package:moni/styles.dart';
import 'package:moni/views/addTutoring/add_tutoring_form.dart';


class AddTutoring extends StatefulWidget {
  const AddTutoring({Key? key}) : super(key: key);

  @override
  _AddTutoringState createState() => _AddTutoringState();
}

class _AddTutoringState extends State<AddTutoring> {

/*   ShakeDetector? detector;
  ShakeController controller = ShakeController();

  @override
  void initState() {
    super.initState();
    detector = ShakeDetector.autoStart(
      onPhoneShake: () {
        controller.showOverlay(context);
        
      },
    );
  }

  @override
  void dispose() {
    detector?.stopListening();
    super.dispose();
  }
 */
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        color: lightGrey,
      ),
      child: ListView(
        children: [
          Row(
            children: [
              Container(
                  alignment: Alignment.center,
                  margin: const EdgeInsets.fromLTRB(0, 30, 0, 40),
                  child: MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      'Back',
                      style: rubik16lightBlue,
                    ),
                  )),
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.fromLTRB(0, 30, 0, 40),
                child: Text(
                  'Create tutoring',
                  style: topViewTitle,
                ),
              ),
            ],
          ),
          AddTutoringForm(),
        ],
      ),
    ));
  }
}
