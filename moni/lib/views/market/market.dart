import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:moni/models/tutoring.dart';
import 'package:moni/repository/tutoring_repository.dart';
import 'package:moni/views/market/card_descubre_fechas.dart';
import 'package:moni/styles.dart';
import 'package:moni/widgets/navbar.dart';
import 'package:flutter/material.dart';
import 'package:shake/shake.dart';

import '../../controller/add_tutoring_controller.dart';
import '../../controller/schedule_tutoring_controller.dart';
import '../../controller/shake_controller.dart';
import '../../network/network_connectivity.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  ShakeDetector? detector;
  ShakeController controller = ShakeController();
  late StreamSubscription<dynamic> stream;


  Map _source = {ConnectivityResult.none: false};
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;
  @override
  void initState() {
    super.initState();
    detector = ShakeDetector.autoStart(
      onPhoneShake: () {
        controller.showOverlay(context);
        // Do stuff on phone shake
      },
    );

    _networkConnectivity.initialise();
    stream = _networkConnectivity.myStream.listen((source) {
        if(!(source.keys.toList()[0] != ConnectivityResult.none && !source.values.toList()[0])){
        //Check and upload pending sessions
        uploadNoInternetSessionsController();
      }
      setState(() => _source = source);
    });
  }

  @override
  void dispose() {
    stream.cancel();
    detector?.stopListening();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: NavBar(),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          color: lightGrey,
        ),
        child: ListView(
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.fromLTRB(0, 30, 0, 40),
              child: Text(
                'Market',
                style: topViewTitle,
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                margin: const EdgeInsets.fromLTRB(25, 0, 0, 20),
                child: Text(
                  'Based on your main interests',
                  style: subtitle,
                ),
              ),
            ),
            Container(
                width: MediaQuery.of(context).size.width / 1.08,
                height: MediaQuery.of(context).size.height / 4.1,
                padding: EdgeInsets.all(0),
                margin: const EdgeInsets.fromLTRB(17, 0, 0, 0),
                child: FutureBuilder<List<Tutoring>>(
                  future: getTutoringByInterestHandler(1),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return ListView(
                        // This next line does the trick.
                        scrollDirection: Axis.horizontal,
                        itemExtent: 135,
                        children: <Widget>[
                          ...?snapshot.data
                              ?.map((e) => CardDescubreFechas(
                                    tutoring: e,
                                  ))
                              .toList()
                        ],
                      );
                    }
                    return Container(
                        margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                        alignment: Alignment.centerLeft,
                        child: Text('No tutorings match your main interests'));
                  },
                )),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                margin: const EdgeInsets.fromLTRB(25, 25, 0, 20),
                child: Text(
                  'Other tutorings you may like',
                  style: subtitle,
                ),
              ),
            ),
            Container(
                width: MediaQuery.of(context).size.width / 1.08,
                height: MediaQuery.of(context).size.height / 4.1,
                padding: EdgeInsets.all(0),
                margin: const EdgeInsets.fromLTRB(17, 0, 0, 0),
                child: FutureBuilder<List<Tutoring>>(
                  future: getTutoringByInterestHandler(2),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return ListView(
                        // This next line does the trick.
                        scrollDirection: Axis.horizontal,
                        itemExtent: 135,
                        children: <Widget>[
                          ...?snapshot.data
                              ?.map((e) => CardDescubreFechas(
                                    tutoring: e,
                                  ))
                              .toList()
                        ],
                      );
                    }
                    return Container(
                        margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                        alignment: Alignment.centerLeft,
                        child: Text('No tutorings match your interests'));
                  },
                )),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                margin: const EdgeInsets.fromLTRB(25, 25, 0, 20),
                child: Text(
                  'Highly requested',
                  style: subtitle,
                ),
              ),
            ),
            Container(
                width: MediaQuery.of(context).size.width / 1.08,
                height: MediaQuery.of(context).size.height / 4.1,
                padding: EdgeInsets.all(0),
                margin: const EdgeInsets.fromLTRB(17, 0, 0, 0),
                child: FutureBuilder<List<Tutoring>>(
                  future: getTutoringsBySessionCount(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData && snapshot.data!.isNotEmpty) {
                      return ListView(
                        // This next line does the trick.
                        scrollDirection: Axis.horizontal,
                        itemExtent: 135,
                        children: <Widget>[
                          ...?snapshot.data
                              ?.map((e) => CardDescubreFechas(
                                    tutoring: e,
                                  ))
                              .toList()
                        ],
                      );
                    }
                    return Container(
                        margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                        alignment: Alignment.centerLeft,
                        child: Text('No highly requested tutorings yet'));
                  },
                )),
            MaterialButton(
              onPressed: () => controller.showOverlay(context),
            )
          ],
        ),
      ),
    );
  }
}
