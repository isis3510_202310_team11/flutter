import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:moni/models/user.dart';

FirebaseFirestore firestore = FirebaseFirestore.instance;

CollectionReference users = FirebaseFirestore.instance.collection('/users');

Future<void> addUserFB(User user) {
  // Call the user's CollectionReference to add a new user
  Map jsonUser = user.toJson();

  return users
      .doc(user.email)
      .set(jsonUser)
      .then((value) => {})
      .catchError((error) => {});
}

Future<User?> getUserFB(String? email) async {
  if (email == null) return null;
  DocumentSnapshot user = await users.doc(email).get();
  return User.fromJson(user.data() as Map<String, dynamic>);
}

Future<void> changeProfilePictureFB(String url, String? email) async {
  if (email == null) return;

  return users
      .doc(email)
      .update({'profilePicture': url})
      .then((value) => print("Profile Picture Updated"))
      .catchError((error) => {});
}
