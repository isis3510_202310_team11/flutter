import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:moni/styles.dart';
import 'package:moni/controller/schedule_tutoring_controller.dart';
import '../../controller/image_controller.dart';

import '../../models/tutoring.dart';

class SeeReview extends StatefulWidget {
  final Tutoring tutoring;

  const SeeReview({
    Key? key,
    required this.tutoring,
  }) : super(key: key);

  @override
  _SeeReview createState() => _SeeReview();
}

class _SeeReview extends State<SeeReview> {
  final TextEditingController _reviewController = TextEditingController();
  double _rating = 0.0; // initialize rating to zero
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        color: lightGrey,
      ),
      child: ListView(
        children: [
          Row(
            children: [
              Container(
                  alignment: Alignment.center,
                  margin: const EdgeInsets.fromLTRB(0, 30, 0, 40),
                  child: MaterialButton(
                    onPressed: () {
                      sessionClearForm();
                      Navigator.pop(context);
                    },
                    child: Text(
                      'Back',
                      style: rubik16lightBlue,
                    ),
                  )),
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.fromLTRB(0, 30, 0, 40),
                child: Text(
                  'Check reviews',
                  style: topViewTitle,
                ),
              ),
            ],
          ),
          Container(
            width: MediaQuery.of(context).size.width / 1.08,
            height: MediaQuery.of(context).size.height / 4,
            margin: const EdgeInsets.fromLTRB(24, 0, 24, 20),
            padding: const EdgeInsets.fromLTRB(24, 5, 24, 0),
            decoration: BoxDecoration(
                color: silverSand,
                borderRadius: BorderRadius.circular(5),
                border: Border.all(width: 0.5, color: silverSand)),
            child: Column(children: [
              Container(
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.asset(
                    getImage(widget.tutoring.topic),
                    height: MediaQuery.of(context).size.height * 0.13,
                    width: MediaQuery.of(context).size.height * 0.13,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Text(
                  widget.tutoring.title,
                  style: rubik16black,
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Text(
                  widget.tutoring.description,
                  style: rubik14black,
                ),
              )
            ]),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: const EdgeInsets.fromLTRB(25, 0, 0, 20),
              child: Text(
                'Calification:',
                style: formTitle,
              ),
            ),
          ),
          Center(
            child: FutureBuilder<double>(
            future: returnScore(widget.tutoring),  // your Future<double> here
            builder: (BuildContext context, AsyncSnapshot<double> snapshot) {
              if (snapshot.hasData) {
                return RatingBar.builder(
                  initialRating: snapshot.data??0,  // Your final rating goes here
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  ignoreGestures: true, // This makes the stars read-only
                  onRatingUpdate: (rating) {}, // Empty callback function
                );
              } else if (snapshot.hasError) {
                return Text('Error: ${snapshot.error}');
              } else {
                // While the future is not yet completed, display a loading indicator
                return CircularProgressIndicator();
              }
            },
          ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: const EdgeInsets.fromLTRB(25, 20, 0, 20),
              child: Text(
                'Reviews:',
                style: formTitle,
              ),
            ),
          ),
        FutureBuilder<List<String>>(
          future: getReviews(widget.tutoring),  // your Future<List<String>> here
          builder: (BuildContext context, AsyncSnapshot<List<String>> snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                shrinkWrap: true, 
                physics: NeverScrollableScrollPhysics(), 
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  return Container(
                    margin: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                    child:Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        snapshot.data![index],
                        style: TextStyle(fontSize: 16.0),
                      ),
                    ),
                  ),);
                },
              );
            } else if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}');
            } else {
              // While the future is not yet completed, display a loading indicator
              return CircularProgressIndicator();
            }
          },
        ),
        ],
      ),
    ));
  }
}
