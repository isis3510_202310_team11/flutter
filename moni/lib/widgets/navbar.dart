import 'package:moni/styles.dart';
import 'package:flutter/material.dart';

import '../controller/navigation_controller.dart';

class NavBar extends StatefulWidget {
  NavBar({Key? key}) : super(key: key);

  @override
  _NavBarState createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(1), topLeft: Radius.circular(1)),
          boxShadow: [
            BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 10),
          ],
        ),
        child: ClipRRect(
          child: BottomNavigationBar(
            onTap: (i) => onItemTapped(context, i),
            showUnselectedLabels: false,
            showSelectedLabels: false,
            iconSize: 30,
            backgroundColor: Color.fromARGB(255, 250, 250, 250),
            type: BottomNavigationBarType.fixed,
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.store,
                    color: softBlue,
                  ),
                  label: 'Home'),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.search,
                    color: softBlue,
                  ),
                  label: 'Search'),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.add_circle_outline,
                    color: softBlue,
                  ),
                  label: 'Add'),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.calendar_month,
                    color: softBlue,
                  ),
                  label: 'Calendar'),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.account_circle,
                    color: softBlue,
                  ),
                  label: 'Profile'),
            ],
          ),
        ));
  }
}
