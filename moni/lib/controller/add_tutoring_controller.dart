import 'package:firebase_auth/firebase_auth.dart' as FirebaseAuth;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import "package:intl/intl.dart";
import 'package:moni/controller/navigation_controller.dart';
import 'package:moni/models/tutoring.dart';
import 'package:moni/repository/tutoring_repository.dart';
import 'package:moni/repository/user_repository.dart';
import '../../models/user.dart';

final TextEditingController titleField = TextEditingController();
final TextEditingController descriptionField = TextEditingController();
final TextEditingController priceField = TextEditingController();
String topicField = '';
String locationField = '';
String? correo = "";

class CurrencyInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    double value = double.parse(newValue.text);

    final formatter = NumberFormat.simpleCurrency(locale: "es_US");

    String newText = formatter.format(value / 100);

    return newValue.copyWith(
        text: newText,
        selection: TextSelection.collapsed(offset: newText.length));
  }
}

void addTutoringHandler(
  BuildContext context,
) async {
  if (titleField.text.isEmpty ||
      descriptionField.text.isEmpty ||
      priceField.text.isEmpty ||
      topicField.isEmpty ||
      locationField.isEmpty) return;

  Tutoring tutoring = Tutoring(
      '1',
      FirebaseAuth.FirebaseAuth.instance.currentUser?.email,
      titleField.text,
      descriptionField.text,
      priceField.text,
      topicField,
      locationField == 'University' ? true : false,
      [],
      [],
      0);

  await addTutoring(tutoring);
  homeTap(context);
}

Future<List<Tutoring>> getTutoringByInterestHandler(int interesNum) async {
  User? user =
      await getUser(FirebaseAuth.FirebaseAuth.instance.currentUser?.email);

  if (user == null) return [];
  switch (interesNum) {
    case 1:
      return getTutoringsByInterest1(user.interes1);
    case 2:
      return getTutoringsByInterest2(user.interes2);
    default:
      return [];
  }
}
