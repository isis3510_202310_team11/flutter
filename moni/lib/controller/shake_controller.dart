import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ShakeController {
  OverlayState? overlayState;
  OverlayEntry? overlayEntry;

  void showOverlay(BuildContext context) async {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return SizedBox(
          height: 300.0,
          child: Column(
            children: [
              SizedBox(height: 70.0),
              Icon(
                Icons.help_outline_rounded,
                size: 30.0,
                color: Color.fromARGB(255, 0, 0, 0),
              ),
              SizedBox(height: 5.0),
              Text(
                'Do you have a problem with your tutor?',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18.0,
                ),
              ),
              SizedBox(height: 1.0),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 40.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Report any issues you may have in order to improve the tutoring experience.',
                        textAlign: TextAlign.center,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 15.0,
                        ),
                      ),
                      SizedBox(height: 20.0),
                      ElevatedButton(
                        onPressed: () {
                          launch("tel://123");
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Color.fromARGB(255, 22, 48, 102),
                          textStyle: TextStyle(fontSize: 15.0),
                          minimumSize: Size(300.0, 50.0),
                        ),
                        child: Text('Report to the police'),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
