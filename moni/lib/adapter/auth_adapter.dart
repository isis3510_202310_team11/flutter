import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

Future<bool> signInFB(String email, String password) async {
  try {
    await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password);
    return true;
  } catch (e) {
    return false;
  }
}

Future<bool> registerFB(String email, String password, String nombre,
    String interes1, String interes2) async {
  var cliente = {
    "email": email,
    "nombre": nombre,
  };
  try {
    await FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email, password: password);
    try {
      var currentUser = FirebaseAuth.instance.currentUser;

      if (currentUser != null) {
        String uid = currentUser.uid;
        DocumentReference documentReference =
            FirebaseFirestore.instance.collection('users').doc(uid);
        FirebaseFirestore.instance.runTransaction((transaction) async {
          transaction.set(documentReference, cliente);
        });
      }
    } catch (e) {
      return false;
    }
    return true;
  } on FirebaseAuthException catch (e) {
    if (e.code == 'weak-password') {
      return false;
    } else if (e.code == 'email-already-in-use') {
      return false;
    }
  } catch (e) {
    return false;
  }
  return false;
}

bool isLoggedInFB() {
  var currentUser = FirebaseAuth.instance.currentUser;

  if (currentUser != null) {
    return true;
  }
  return false;
}
