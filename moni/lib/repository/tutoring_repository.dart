import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:moni/adapter/tutoring_adapter.dart';
import 'package:moni/models/tutoring.dart';

Future<void> addTutoring(Tutoring tutoring) {
  return addTutoringFB(tutoring);
}

Future<Tutoring?> getTutoring(String? id, GetOptions options) async {
  return getTutoringFB(id, options);
}

Future<List<Tutoring>> getTutoringsByLocation(bool inUniversity) async {
  return getTutoringsByLocationFB(inUniversity);
}

Future<List<Tutoring>> getTutoringsByInterest1(String interest1) async {
  return getTutoringsByInterest1FB(interest1);
}

Future<List<Tutoring>> getTutoringsByInterest2(String interest1) async {
  return getTutoringsByInterest2FB(interest1);
}

//Get tutorings ordered by sessionCount
Future<List<Tutoring>> getTutoringsBySessionCount() async {
  return getTutoringsBySessionCountFB();
}

//Book tutoring update sessionCount
Future<void> bookTutoring(String? id, Tutoring tutoring) async {
  return bookTutoringFB(id, tutoring);
}

//Update reviews
Future<void> updateReviews(Tutoring tutoring) async {
  return updateReviewsFB(tutoring);
}

//Get reviews
Future<List<String>> getReviews(Tutoring tutoring) async {
  return getReviewsFB(tutoring);
}

//Get scores
Future<List<double>> getScores(Tutoring tutoring) async {
  return getScoresFB(tutoring);
}


