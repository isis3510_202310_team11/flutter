import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelperReview {
  static const _databaseName = "ReviewsLocalStorage.db";
  static const _databaseVersion = 1;

  static const table = 'reviews';

  static const columnTutoringId = 'tutoringId';
  static const columnReview = 'review';
  static const columnScore = 'score';


  // Singleton class
  DatabaseHelperReview._privateConstructor();
  static final DatabaseHelperReview instance = DatabaseHelperReview._privateConstructor();

  // Database reference
  static Database? _database;
  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDatabase();
    return _database!;
  }

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $table (
            $columnTutoringId TEXT,
            $columnReview TEXT,
            $columnScore TEXT
          )
          ''');
  }
}