import 'package:flutter/material.dart';
import 'package:moni/models/user.dart';
import 'package:moni/repository/user_repository.dart';
import '../repository/auth_repository.dart';
import 'navigation_controller.dart';

final TextEditingController emailField = TextEditingController();
final TextEditingController nameField = TextEditingController();
final TextEditingController passwordField = TextEditingController();
final TextEditingController confirmPasswordField = TextEditingController();
final TextEditingController interest1Field = TextEditingController();
String interest1 = '';
String interest2 = '';

void registerHandler(BuildContext context) async {
  bool shouldNavigate = await register(
    emailField.text,
    passwordField.text,
    nameField.text,
    interest1,
    interest2,
  );
  User user = User(emailField.text, nameField.text, interest1, interest2,
      'https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg');

  if (shouldNavigate) {
    await addUser(user);
    homeTap(context);
  } else {
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(content: Text('Email already in use')),
    );
  }
}
