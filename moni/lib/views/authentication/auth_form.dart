import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:moni/controller/auth_controller.dart';
import 'package:moni/controller/navigation_controller.dart';
import 'package:moni/network/network_connectivity.dart';
import 'package:moni/styles.dart';

class AuthForm extends StatefulWidget {
  const AuthForm({super.key});

  @override
  State<AuthForm> createState() => _AuthFormState();
}

class _AuthFormState extends State<AuthForm> {
  var visibilityPW = false;
  final _formKey = GlobalKey<FormState>();
  late StreamSubscription<dynamic> stream;

  Map _source = {ConnectivityResult.none: false};
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;
  String string = '';

  @override
  void initState() {
    super.initState();
    _networkConnectivity.initialise();
    stream = _networkConnectivity.myStream.listen((source) {
      setState(() => _source = source);
    });
  }

  @override
  void dispose() {
    stream.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.fromLTRB(0, 70, 0, 50),
            child: Text('Log In', style: topViewTitle),
          ),
          Container(
            width: MediaQuery.of(context).size.width / 1.08,
            margin: const EdgeInsets.fromLTRB(0, 0, 0, 21),
            padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
            decoration: BoxDecoration(
                color: lightGrey,
                borderRadius: BorderRadius.circular(5),
                boxShadow: shadowInputFields),
            child: TextFormField(
              cursorColor: darkBlue,
              style: rubik18darkPurple,
              controller: emailField,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter your email';
                }
                return null;
              },
              decoration: InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: darkBlue, width: 3)),
                  labelText: 'Email',
                  labelStyle: rubik18darkPurple,
                  suffixIcon: Icon(
                    Icons.email_outlined,
                    color: softBlue,
                  )),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width / 1.08,
            margin: const EdgeInsets.fromLTRB(0, 0, 0, 12),
            padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
            decoration: BoxDecoration(
                color: lightGrey,
                borderRadius: BorderRadius.circular(5),
                boxShadow: shadowInputFields),
            child: TextFormField(
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter your password';
                }
                return null;
              },
              cursorColor: darkBlue,
              style: rubik18darkPurple,
              controller: passwordField,
              obscureText: !visibilityPW,
              decoration: InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: darkBlue, width: 3)),
                  labelText: 'Password',
                  labelStyle: rubik18darkPurple,
                  suffixIcon: IconButton(
                    icon: Icon(
                      visibilityPW
                          ? Icons.visibility_outlined
                          : Icons.visibility_off_outlined,
                      color: softBlue,
                    ),
                    onPressed: () => setState(() {
                      visibilityPW = !visibilityPW;
                    }),
                  )),
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width / 1.38,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  'Forgot your password?',
                  style: rubik13black,
                )
              ],
            ),
          ),
          (_source.keys.toList()[0] != ConnectivityResult.none &&
                  !_source.values.toList()[0])
              ? MaterialButton(
                  onPressed: () => {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                              content: Text('No internet connection')),
                        ),
                      },
                  child: Container(
                      margin: const EdgeInsets.fromLTRB(0, 150, 0, 20),
                      padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                      width: MediaQuery.of(context).size.width / 1.08,
                      decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(5),
                          boxShadow: shadowInputFields),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Can\'t connect to the internet',
                            style: rubik16white,
                          ),
                        ],
                      )))
              : MaterialButton(
                  onPressed: () => {
                        if (_formKey.currentState!.validate())
                          {
                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(content: Text('Logging in...')),
                            ),
                            loginHandler(context),
                          }
                      },
                  child: Container(
                      margin: const EdgeInsets.fromLTRB(0, 150, 0, 20),
                      padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                      width: MediaQuery.of(context).size.width / 1.08,
                      decoration: BoxDecoration(
                          color: darkBlue,
                          borderRadius: BorderRadius.circular(5),
                          boxShadow: shadowInputFields),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Log In',
                            style: rubik16white,
                          ),
                        ],
                      ))),
          RichText(
              text: TextSpan(style: rubik16black, children: [
            const TextSpan(
              text: 'New to moni? ',
            ),
            TextSpan(
              text: 'Register',
              style: rubik16darkBlue,
              recognizer: TapGestureRecognizer()
                ..onTap = () => registerTap(context),
            ),
          ])),
        ],
      )),
    );
  }
}
