import 'dart:async';
import 'package:flutter/material.dart';
import 'package:moni/controller/schedule_tutoring_controller.dart';
import 'package:moni/styles.dart';
import '../../network/network_connectivity.dart';
import 'package:connectivity_plus/connectivity_plus.dart';


class ScheduleTutoringForm extends StatefulWidget {
  final String tutoringPrice;
  final String tutoringId;

  const ScheduleTutoringForm(
      {Key? key, required this.tutoringPrice, required this.tutoringId})
      : super(key: key);

  @override
  _ScheduleTutoringFormState createState() => _ScheduleTutoringFormState();
}

class _ScheduleTutoringFormState extends State<ScheduleTutoringForm> {
  final _formKey = GlobalKey<FormState>();
  late DateTime _selectedDate;
  late TimeOfDay _selectedTime;
  late String _selectedPlace;
  late StreamSubscription<dynamic> stream;

  Map _source = {ConnectivityResult.none: false};
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;

  @override
  void initState() {
    super.initState();

    _selectedDate = DateTime.now();
    dateField.text =
        '${_selectedDate.day}/${_selectedDate.month}/${_selectedDate.year}';
    _selectedTime = TimeOfDay.now();
    timeField.text = '${_selectedTime.hour}:${_selectedTime.minute}';
    _selectedPlace = '';

    _networkConnectivity.initialise();
    stream = _networkConnectivity.myStream.listen((source) {
      if (!(source.keys.toList()[0] != ConnectivityResult.none &&
          !source.values.toList()[0])) {
        //Check and upload pending sessions
        uploadNoInternetSessionsController();
      }
      setState(() => _source = source);
    });
  }

  @override
  void dispose() {
    stream.cancel();
    super.dispose();
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: _selectedDate,
        firstDate: DateTime(2021),
        lastDate: DateTime(2025));
    if (picked != null && picked != _selectedDate) {
      setState(() {
        _selectedDate = picked;
      });
      dateField.text =
          '${_selectedDate.day}/${_selectedDate.month}/${_selectedDate.year}';
    }
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay? picked =
        await showTimePicker(context: context, initialTime: _selectedTime);
    if (picked != null && picked != _selectedTime) {
      setState(() {
        _selectedTime = picked;
      });

      timeField.text = '${_selectedTime.hour}:${_selectedTime.minute}';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          children: [
            Container(
                width: MediaQuery.of(context).size.width / 1.08,
                margin: const EdgeInsets.fromLTRB(24, 0, 24, 20),
                padding: const EdgeInsets.fromLTRB(24, 5, 24, 0),
                decoration: BoxDecoration(
                    color: verylightGrey,
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(width: 0.5, color: verylightGrey)),
                // create a form that takes a date, a time and a place, make the date and time datepickers and display tutoring price at the top.
                child: Column(
                  children: [
                    Container(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              const Text('Date:'),
                              const SizedBox(width: 16.0),
                              ElevatedButton(
                                onPressed: () => _selectDate(context),
                                child: Text(
                                    '${_selectedDate.year}-${_selectedDate.month}-${_selectedDate.day}'),
                              ),
                            ],
                          ),
                          const SizedBox(height: 16.0),
                          Row(
                            children: [
                              const Text('Time:'),
                              const SizedBox(width: 16.0),
                              ElevatedButton(
                                onPressed: () => _selectTime(context),
                                child: Text(
                                    '${_selectedTime.hour}:${_selectedTime.minute}'),
                              ),
                            ],
                          ),
                          const SizedBox(height: 16.0),
                          TextFormField(
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please enter a place';
                              }
                              return null;
                            },
                            onChanged: (value) {
                              setState(() {
                                _selectedPlace = value;
                              });
                            },
                            decoration: InputDecoration(
                              labelText: 'Place',
                              border: OutlineInputBorder(),
                            ),
                            controller: placeField,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(16.0),
                      child: Text(
                        'Tutoring Price: ${widget.tutoringPrice}',
                        style: TextStyle(fontSize: 14.0),
                      ),
                    ),
                  ],
                )),
            MaterialButton(
              onPressed: () async => {
                if (_formKey.currentState!.validate())
                  {
                    // If the form is valid, display a snackbar. In the real world,
                    // you'd often call a server or save the information in a database.
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        content: Text('Scheduling tutoring...'))),
                    //Shows user that it cant be scheduled yet.

                    if ((_source.keys.toList()[0] != ConnectivityResult.none &&
                        !_source.values.toList()[0]))
                      {
                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                            content: Text(
                                'There is no internet connection, we will book it when it comes back, check your email for the update.'))),
                      },
                    addSessionHandler(
                        context,
                        widget.tutoringId,
                        (_source.keys.toList()[0] != ConnectivityResult.none &&
                            !_source.values.toList()[0]))
                  }
              },
              child: Container(
                  margin: const EdgeInsets.fromLTRB(0, 14, 0, 20),
                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                  width: MediaQuery.of(context).size.width / 1.08,
                  decoration: BoxDecoration(
                      color: darkBlue,
                      borderRadius: BorderRadius.circular(5),
                      boxShadow: shadowInputFields),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Create',
                        style: rubik16white,
                      ),
                    ],
                  )),
            ),
          ],
        ));
  }
}
