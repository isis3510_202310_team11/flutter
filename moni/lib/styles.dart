import 'package:flutter/material.dart';

const String rubik = 'Rubik';

Color slateBlue = const Color(0xFF725ac1);
Color darkBlue = Color.fromARGB(255, 23, 48, 102);
Color lightBlue = Color.fromARGB(255, 39, 80, 168);
Color softBlue = Color.fromARGB(255, 159, 181, 226);
Color lightGrey = const Color.fromARGB(255, 246, 246, 246);
Color verylightGrey = const Color.fromARGB(255, 222, 220, 220);
Color formGrey = Color.fromARGB(255, 232, 232, 232);
Color darkGrey = const Color.fromARGB(255, 180, 180, 180);
Color silverSand = const Color(0xFFc4cad0);
Color yellow = Color.fromARGB(255, 255, 184, 78);

TextStyle topViewTitle = TextStyle(
  fontFamily: rubik,
  fontWeight: FontWeight.w600,
  fontSize: 35,
  color: lightBlue,
);

TextStyle topViewTitleWhite = TextStyle(
  fontFamily: rubik,
  fontWeight: FontWeight.w600,
  fontSize: 35,
  color: Colors.white,
);

TextStyle subtitle = TextStyle(
  fontFamily: rubik,
  fontWeight: FontWeight.w500,
  fontSize: 28,
  color: Colors.black,
);

TextStyle formTitle = TextStyle(
    fontFamily: rubik,
    fontWeight: FontWeight.w500,
    fontSize: 23,
    color: Colors.black);

TextStyle rubik18darkPurple = TextStyle(
  fontFamily: rubik,
  fontWeight: FontWeight.w400,
  fontSize: 18,
  color: Colors.black,
);

TextStyle rubik16white = TextStyle(
  fontFamily: rubik,
  fontWeight: FontWeight.w600,
  fontSize: 16,
  color: Colors.white,
);

TextStyle rubik16black = TextStyle(
  fontFamily: rubik,
  fontWeight: FontWeight.w600,
  fontSize: 16,
  color: Colors.black,
);

TextStyle rubik16darkBlue = TextStyle(
  fontFamily: rubik,
  fontWeight: FontWeight.w600,
  fontSize: 16,
  color: darkBlue,
);

TextStyle rubik16lightBlue = TextStyle(
  fontFamily: rubik,
  fontWeight: FontWeight.w600,
  fontSize: 16,
  color: lightBlue,
);

TextStyle rubik13black = TextStyle(
  fontFamily: rubik,
  fontWeight: FontWeight.w400,
  fontSize: 13,
  color: Colors.black,
);

TextStyle rubik13darkPurple = TextStyle(
  fontFamily: rubik,
  fontWeight: FontWeight.w400,
  fontSize: 13,
  color: Colors.black,
);
TextStyle rubik14black = TextStyle(
  fontFamily: rubik,
  fontWeight: FontWeight.w400,
  fontSize: 14,
  color: Colors.black,
);

TextStyle rubik14grey = TextStyle(
  fontFamily: rubik,
  fontWeight: FontWeight.w400,
  fontSize: 14,
  color: darkGrey,
);

TextStyle rubik14blackBold = TextStyle(
  fontFamily: rubik,
  fontWeight: FontWeight.w700,
  fontSize: 14,
  color: Colors.black,
);

List<BoxShadow> shadowInputFields = [
  BoxShadow(
    color: Colors.black.withOpacity(0.25),
    spreadRadius: 0,
    blurRadius: 4,
    offset: const Offset(0, 4), // changes position of shadow
  )
];
