import 'package:moni/adapter/user_adapter.dart';
import 'package:moni/models/user.dart';

Future<void> addUser(User user) {
  return addUserFB(user);
}

Future<User?> getUser(String? email) async {
  return getUserFB(email);
}

Future<void> changeProfilePicture(String url, String? email) async {
  return changeProfilePictureFB(url, email);
}
