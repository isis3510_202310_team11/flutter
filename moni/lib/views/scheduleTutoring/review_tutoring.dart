import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:moni/controller/navigation_controller.dart';
import 'package:moni/styles.dart';
import 'package:moni/controller/schedule_tutoring_controller.dart';
import '../../controller/image_controller.dart';

import '../../models/tutoring.dart';
import '../../network/network_connectivity.dart';

class ReviewTutoring extends StatefulWidget {
  final Tutoring tutoring;

  const ReviewTutoring({
    Key? key,
    required this.tutoring,
  }) : super(key: key);

  @override
  _ReviewTutoring createState() => _ReviewTutoring();
}

class _ReviewTutoring extends State<ReviewTutoring> {
  final TextEditingController _reviewController = TextEditingController();

//Connection
  late StreamSubscription<dynamic> stream;
  Map _source = {ConnectivityResult.none: false};
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;

  @override
  void initState() {
    super.initState();

    _networkConnectivity.initialise();
    stream = _networkConnectivity.myStream.listen((source) {
      if (!(source.keys.toList()[0] != ConnectivityResult.none &&
          !source.values.toList()[0])) {
        //Check and upload pending review
        uploadNoInternetReviewsController();
      }
      setState(() => _source = source);
    });
  }

  @override
  void dispose() {
    stream.cancel();
    super.dispose();
  }

  double _rating = 0.0; // initialize rating to zero
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        color: lightGrey,
      ),
      child: ListView(
        children: [
          Row(
            children: [
              Container(
                  alignment: Alignment.center,
                  margin: const EdgeInsets.fromLTRB(0, 30, 0, 40),
                  child: MaterialButton(
                    onPressed: () {
                      sessionClearForm();
                      Navigator.pop(context);
                    },
                    child: Text(
                      'Back',
                      style: rubik16lightBlue,
                    ),
                  )),
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.fromLTRB(0, 30, 0, 40),
                child: Text(
                  'Review tutoring',
                  style: topViewTitle,
                ),
              ),
            ],
          ),
          Container(
            width: MediaQuery.of(context).size.width / 1.08,
            height: MediaQuery.of(context).size.height / 4,
            margin: const EdgeInsets.fromLTRB(24, 0, 24, 20),
            padding: const EdgeInsets.fromLTRB(24, 5, 24, 0),
            decoration: BoxDecoration(
                color: silverSand,
                borderRadius: BorderRadius.circular(5),
                border: Border.all(width: 0.5, color: silverSand)),
            child: Column(children: [
              Container(
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.asset(
                    getImage(widget.tutoring.topic),
                    height: MediaQuery.of(context).size.height * 0.13,
                    width: MediaQuery.of(context).size.height * 0.13,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Text(
                  widget.tutoring.title,
                  style: rubik16black,
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Text(
                  widget.tutoring.description,
                  style: rubik14black,
                ),
              )
            ]),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: const EdgeInsets.fromLTRB(25, 0, 0, 20),
              child: Text(
                'Calification:',
                style: formTitle,
              ),
            ),
          ),
          Center(
            child: RatingBar.builder(
              initialRating: 0,
              minRating: 1,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
              itemBuilder: (context, _) => Icon(
                Icons.star,
                color: Colors.amber,
              ),
              onRatingUpdate: (rating) {
                setState(() {
                  _rating = rating; // update the rating based on user's choice
                });
              },
            ),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: const EdgeInsets.fromLTRB(25, 20, 0, 20),
              child: Text(
                'Comments:',
                style: formTitle,
              ),
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width / 1.08,
              height: MediaQuery.of(context).size.height / 8,
              margin: const EdgeInsets.fromLTRB(24, 0, 24, 20),
              padding: const EdgeInsets.fromLTRB(24, 5, 24, 0),
              decoration: BoxDecoration(
                  color: formGrey,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(width: 0.5, color: darkGrey)),
              child: TextField(
                controller: _reviewController,
                maxLines: null,
                decoration: InputDecoration(
                    border: InputBorder.none, hintText: "I liked that ..."),
              )),
          MaterialButton(
            onPressed: () async => {
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(content: Text('Saving review...')),
              ),
              //Shows user that it cant be sent yet.
              if ((_source.keys.toList()[0] != ConnectivityResult.none &&
                  !_source.values.toList()[0]))
                {
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                      content: Text(
                          'There is no internet connection, we will upload the review when it comes back, check your email for the update.'))),
                },
              addReviewandScore(
                  (_source.keys.toList()[0] != ConnectivityResult.none &&
                      !_source.values.toList()[0]),
                  context,
                  _reviewController.text,
                  _rating,
                  widget.tutoring),
              _reviewController.clear(),
              Navigator.pushNamed(context, homeRoute) // clear the text field
            },
            child: Container(
                margin: const EdgeInsets.fromLTRB(25, 14, 25, 35),
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                width: MediaQuery.of(context).size.width / 1.08,
                decoration: BoxDecoration(
                    color: darkBlue,
                    borderRadius: BorderRadius.circular(5),
                    boxShadow: shadowInputFields),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Send Review',
                      style: rubik16white,
                    ),
                  ],
                )),
          ),
        ],
      ),
    ));
  }
}
