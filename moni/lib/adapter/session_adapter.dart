import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:moni/models/session.dart';
import 'package:intl/intl.dart';

import '../models/tutoring.dart';

class SessionTutoring {
  final Session session;
  final Tutoring? tutoring;

  SessionTutoring(this.session, this.tutoring);
}

FirebaseFirestore firestore = FirebaseFirestore.instance;

CollectionReference sessions =
    FirebaseFirestore.instance.collection('sessions');

Future<void> bookSessionFB(Session session) {
  // Call the user's CollectionReference to add a new user
  Map jsonSession = session.toJson();

  return sessions
      .add(jsonSession)
      .then((value) => {})
      .catchError((error) => {});
}

Future<Map<String, List<Session>>> getSessionsForUserFB() async {
  try {
    String? email = FirebaseAuth.instance.currentUser?.email;
    QuerySnapshot clientSessions = await sessions
        .where('clientEmail', isEqualTo: email)
        .get(const GetOptions(source: Source.serverAndCache));

    Map<String, List<Session>> map = {};

    for (var item in clientSessions.docs) {
      Session session = Session.fromJson(item.data() as Map<String, dynamic>);
      String dateISO = DateFormat.yMd().format(session.meetingDate);

      if (map.containsKey(dateISO)) {
        map[dateISO]?.add(session);
      } else {
        map[dateISO] = [session];
      }
    }

    return map;
  } catch (e) {
    return {};
  }
}

Future<List<Session>> getSessionsForUserByDateFB(
    String? email, DateTime date) async {
  try {
    QuerySnapshot clientSessions = await sessions
        .where('clientEmail', isEqualTo: email)
        .where('meetingDate',
            isGreaterThan: DateTime(date.year, date.month, date.day))
        .where('meetingDate',
            isLessThan: DateTime(date.year, date.month, date.day, 24, 59, 59))
        .orderBy('meetingDate')
        .get(const GetOptions(source: Source.serverAndCache));

    return clientSessions.docs.map((DocumentSnapshot document) {
      return Session.fromJson(document.data() as Map<String, dynamic>);
    }).toList();
  } catch (e) {
    return [];
  }
}
