import 'package:moni/adapter/auth_adapter.dart';

Future<bool> signIn(String email, String password) async {
  return signInFB(email, password);
}

Future<bool> register(String email, String password, String nombre,
    String interes1, String interes2) async {
  return registerFB(email, password, nombre, interes1, interes2);
}

bool isLoggedIn() {
  return isLoggedInFB();
}
