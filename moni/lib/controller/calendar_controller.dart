import 'package:firebase_auth/firebase_auth.dart';
import 'package:moni/repository/session_repository.dart';
import 'package:intl/intl.dart';

import '../models/session.dart';

Future<List<Session>> getSessionsForDay(DateTime day) async {
  List<Session> sessiones = await getSessionsForUserByDate(
      FirebaseAuth.instance.currentUser!.email, day);
  return sessiones;
}

//From map of sessions, find list of sessions from before todays date
List<Session> sessionsBeforeToday(Map<String, List<Session>> sessions) {
  List<Session> pastSessions = [];
  DateTime today = DateTime.now();
  sessions.forEach((key, value) {
    if (DateFormat.yMd().parse(key).isBefore(today)) {
      pastSessions.addAll(value);
    }
  });
  return pastSessions;
}

//From map of sessions, find list of sessions from after todays date
List<Session> sessionsAfterToday(Map<String, List<Session>> sessions) {
  List<Session> futureSessions = [];
  DateTime today = DateTime.now();
  sessions.forEach((key, value) {
    if (DateFormat.yMd().parse(key).isAfter(today)) {
      futureSessions.addAll(value);
    }
  });
  return futureSessions;
}

//From map find next session
Session? nextSession(Map<String, List<Session>> sessions) {
  DateTime today = DateTime.now();
  Session? next;
  sessions.forEach((key, value) {
    if (DateFormat.yMd().parse(key).isAfter(today)) {
      if (next == null) {
        next = value[0];
      } else if (value[0].meetingDate.isBefore(next!.meetingDate)) {
        next = value[0];
      }
    }
  });
  return next;
}
