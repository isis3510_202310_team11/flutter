import '../models/sessionNoInternet.dart';
import 'database_helper.dart';

class SessionDao {
  final dbHelper = DatabaseHelper.instance;

  Future<int> insert(SessionNoInternet session) async {
    final db = await dbHelper.database;
    return await db.insert(DatabaseHelper.table, session.toMap());
  }

  Future<List<SessionNoInternet>> queryAll() async {
    final db = await dbHelper.database;
    List<Map<String, dynamic>> maps = await db.query(DatabaseHelper.table);
    return maps.map((map) => SessionNoInternet.fromMap(map)).toList();
  }

  Future<int> deleteAll() async {
    final db = await dbHelper.database;
    return await db.delete(DatabaseHelper.table);
  }
}
