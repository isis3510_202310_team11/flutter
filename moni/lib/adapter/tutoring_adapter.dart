import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:moni/models/tutoring.dart';

FirebaseFirestore firestore = FirebaseFirestore.instance;

CollectionReference tutorings =
    FirebaseFirestore.instance.collection('/tutorings');

Future<void> addTutoringFB(Tutoring tutoring) {
  // Call the user's CollectionReference to add a new user

  Map jsonTutoring = tutoring.toJson();

  return tutorings
      .add(jsonTutoring)
      .then((value) => {})
      .catchError((error) => {});
}

Future<Tutoring?> getTutoringFB(String? id, GetOptions options) async {
  DocumentSnapshot tutoring = await tutorings.doc(id).get(options);
  if (tutoring.data() == null) return null;
  Tutoring tutoringRes =
      Tutoring.fromJson(tutoring.data() as Map<String, dynamic>);
  tutoringRes.id = tutoring.id;
  return tutoringRes;
}

Future<List<Tutoring>> getTutoringsFB(List<String?> ids) async {
  QuerySnapshot batchTutorings = await tutorings
      .where(FieldPath.documentId, whereIn: ids)
      .get(const GetOptions(source: Source.serverAndCache));
  return batchTutorings.docs.map((DocumentSnapshot document) {
    Tutoring tutoring =
        Tutoring.fromJson(document.data() as Map<String, dynamic>);
    tutoring.id = document.id;
    return tutoring;
  }).toList();
}

Future<List<Tutoring>> getTutoringsByLocationFB(bool inUniversity) async {
  QuerySnapshot tutoringsByLocation = await tutorings
      .where('inUniversity', isEqualTo: inUniversity)
      .get(const GetOptions(source: Source.serverAndCache));

  return tutoringsByLocation.docs.map((DocumentSnapshot document) {
    Tutoring tutoring =
        Tutoring.fromJson(document.data() as Map<String, dynamic>);
    tutoring.id = document.id;
    return tutoring;
  }).toList();
}

Future<List<Tutoring>> getTutoringsByInterest1FB(String interest1) async {
  QuerySnapshot tutoringsByInterest1 = await tutorings
      .where('topic', isEqualTo: interest1)
      .get(const GetOptions(source: Source.serverAndCache));

  List<Tutoring> tutoringList =
      tutoringsByInterest1.docs.map((DocumentSnapshot document) {
    Tutoring tutoring =
        Tutoring.fromJson(document.data() as Map<String, dynamic>);
    tutoring.id = document.id;
    return tutoring;
  }).toList();

  tutoringList.removeWhere((tutoring) => !tutoring.price.contains("\$"));
  return tutoringList;
}

Future<List<Tutoring>> getTutoringsByInterest2FB(String interest1) async {
  QuerySnapshot tutoringsByInterest2 = await tutorings
      .where('topic', isEqualTo: interest1)
      .get(const GetOptions(source: Source.serverAndCache));

  List<Tutoring> list =
      tutoringsByInterest2.docs.map((DocumentSnapshot document) {
    Tutoring tutoring =
        Tutoring.fromJson(document.data() as Map<String, dynamic>);
    tutoring.id = document.id;
    return tutoring;
  }).toList();

  list.removeWhere((tutoring) => !tutoring.price.contains("\$"));
  return list;
}

//Get tutorings ordered by sessionCount
Future<List<Tutoring>> getTutoringsBySessionCountFB() async {
  QuerySnapshot tutoringsBySessionCount = await tutorings
      .orderBy('sessionCount', descending: true)
      .limit(10)
      .get(const GetOptions(source: Source.serverAndCache));

  return tutoringsBySessionCount.docs.map((DocumentSnapshot document) {
    Tutoring tutoring =
        Tutoring.fromJson(document.data() as Map<String, dynamic>);
    tutoring.id = document.id;
    return tutoring;
  }).toList();
}

//Update tutoring to increase sessionCount
Future<void> bookTutoringFB(String? id, Tutoring tutoring) {
  Map jsonTutoring = tutoring.toJson();

  return tutorings
      .doc(id)
      .set(jsonTutoring)
      .then((value) => {})
      .catchError((error) => {});
}

//Update tutoring to put reviews
Future<void> updateReviewsFB(Tutoring tutoring) {
  Map jsonTutoring = tutoring.toJson();

  return tutorings
      .doc(tutoring.id)
      .set(jsonTutoring)
      .then((value) => {})
      .catchError((error) => {});
}

//Get reviews
Future<List<String>> getReviewsFB(Tutoring tutoring) async {
  try {
    DocumentSnapshot documentSnapshot = await FirebaseFirestore.instance
        .collection('tutorings')
        .doc(tutoring.id)
        .get();

    if (documentSnapshot.exists) {
      var reviews = List<String>.from(documentSnapshot.get('reviews'));
      return reviews;
    } else {
      return [];
    }
  } catch (error) {
    return []; // Or however you want to handle this error
  }
}

//Get scores
Future<List<double>> getScoresFB(Tutoring tutoring) async {
  try {
    DocumentSnapshot documentSnapshot = await FirebaseFirestore.instance
        .collection('tutorings')
        .doc(tutoring.id)
        .get();

    if (documentSnapshot.exists) {
      var scores = List<double>.from(documentSnapshot.get('scores'));
      return scores;
    } else {
      return [];
    }
  } catch (error) {
    return []; // Or however you want to handle this error
  }
}
