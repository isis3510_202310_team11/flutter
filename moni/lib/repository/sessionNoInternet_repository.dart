import '../DAO/session_dao.dart';
import '../models/sessionNoInternet.dart';
import 'dart:async';

final sessionDao = SessionDao(); // Instantiate your DAO class

Future<SessionNoInternet?> uploadNoInternetSessions() async {
  List<SessionNoInternet> result = await sessionDao.queryAll();
  if (result.isNotEmpty) {
    SessionNoInternet sessionNIToUpload = result.removeLast();
    return sessionNIToUpload;
  } else {
    return null;
  }
}

Future<SessionNoInternet?> deleteNoInternetSessions() async {
  await sessionDao.deleteAll();
  return null;
}
