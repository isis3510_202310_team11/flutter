import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:moni/controller/navigation_controller.dart';
import 'package:moni/models/session.dart';
import 'package:moni/models/tutoring.dart';
import 'package:moni/adapter/tutoring_adapter.dart';
import 'package:intl/intl.dart';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server.dart';
import 'package:moni/repository/reviewNoInternet_repository.dart';
import 'package:moni/repository/session_repository.dart';
import 'package:moni/repository/tutoring_repository.dart';
import 'package:moni/app.dart';

import '../models/reviewNoInternet.dart';
import '../models/sessionNoInternet.dart';
import '../repository/sessionNoInternet_repository.dart';

final TextEditingController dateField = TextEditingController();
final TextEditingController timeField = TextEditingController();
final TextEditingController placeField = TextEditingController();

//FirebaseAnalytics analytics = FirebaseAnalytics();

void _logButtonClick() {
  App.analytics.logEvent(name: 'ScheduleSession');
}

void sessionClearForm() {
  dateField.clear();
  timeField.clear();
  placeField.clear();
}

void addSessionHandler(
    BuildContext context, String? tutoringId, bool connection) async {
  if (dateField.text.isEmpty ||
      timeField.text.isEmpty ||
      placeField.text.isEmpty) return;

  if (connection == false) {
    Tutoring? tutoring = await getTutoring(
        tutoringId, GetOptions(source: Source.serverAndCache));

    tutoring?.sessionCount += 1;

    DateTime date = DateFormat('dd/MM/yyyy HH:mm')
        .parse('${dateField.text.trim()} ${timeField.text.trim()}');
    Session session = Session(
        tutoringId!,
        tutoring!.tutorEmail,
        FirebaseAuth.instance.currentUser!.email,
        DateFormat('dd/MM/yyyy HH:mm')
            .parse('${dateField.text.trim()} ${timeField.text.trim()}'),
        placeField.text);
    sendEmail(tutoring.tutorEmail, timeField.text, date, placeField.text,
        FirebaseAuth.instance.currentUser!.email);
    await bookSession(session, connection);
    sessionClearForm();
    homeTap(context);
  } else {
    SessionNoInternet session = SessionNoInternet(tutoringId!,
        ('${dateField.text.trim()} ${timeField.text.trim()}'), placeField.text);
    saveSessionNoInternet(session, connection);
  }
}

void sendEmail(String? recipient, String time, DateTime date, String place,
    String? user) async {
  String username = 'moniappuniandes@gmail.com';
  String password = 'vgfkrjnswrxjwtwn';
  final smtpServer = gmail(username, password);

  final message = Message()
    ..from = Address(username)
    ..recipients.add(recipient)
    ..subject = 'You have a new session scheduled!'
    ..text =
        'Congratulations!\n\nYou have a new scheduled session at $time $date in $place from the user $user.\n\nWe hope you have a great learning time filled with knowledge!\n\nGreetings,\n\nMoni';

  try {
    final sendReport = await send(message, smtpServer);
  } on MailerException {}
}

void sendEmailReconnection(String? recipient, String time, DateTime date,
    String place, String? user) async {
  String username = 'moniappuniandes@gmail.com';
  String password = 'vgfkrjnswrxjwtwn';
  final smtpServer = gmail(username, password);

  final message = Message()
    ..from = Address(username)
    ..recipients.add(recipient)
    ..subject = 'Moni regained connection! Your session has been scheduled!'
    ..text =
        'Congratulations!\n\nYou have a new scheduled session at $time $date in $place.\n\nWe hope you have a great learning time filled with knowledge!\n\nGreetings,\n\nMoni';

  try {
    final sendReport = await send(message, smtpServer);
  } on MailerException {}
}

void uploadNoInternetSessionsController() async {
  SessionNoInternet? sessionNoInternet = await uploadNoInternetSessions();

  if (sessionNoInternet != null) {
    Tutoring? tutoring = await getTutoring(sessionNoInternet.tutoringId,
        GetOptions(source: Source.serverAndCache));

    DateTime date =
        DateFormat('dd/MM/yyyy HH:mm').parse(sessionNoInternet.meetingDate);

    Session session = Session(
        sessionNoInternet.tutoringId,
        tutoring!.tutorEmail,
        FirebaseAuth.instance.currentUser!.email,
        date,
        sessionNoInternet.place);

    sendEmailReconnection(
        FirebaseAuth.instance.currentUser!.email,
        timeField.text,
        date,
        placeField.text,
        FirebaseAuth.instance.currentUser!.email);

    await bookSession(session, false);

    deleteNoInternetSessions();
    sessionClearForm();
  }
}

void addReviewandScore(bool connection, BuildContext context, String review,
    double score, Tutoring tutoring) async {
  if (connection == false) {
    tutoring.reviews.add(review);
    tutoring.scores.add(score);
    await updateReviews(tutoring);
    homeTap(context);
  } else {
    ReviewNoInternet reviewNoInternet =
        ReviewNoInternet(tutoring.id!, review, score.toString());
    saveReviewNoInternet(reviewNoInternet, connection);
  }
}

Future<double> returnScore(Tutoring tutoring) async {
  List<double> scores = await getScores(tutoring);
  if (scores.isEmpty) {
    return 0;
  }
  double sum = scores.reduce((a, b) => a + b);
  return sum / scores.length;
}

Future<List<String>> getReviews(Tutoring tutoring) async {
  return getReviewsFB(tutoring);
}

void uploadNoInternetReviewsController() async {
  ReviewNoInternet? reviewNoInternet = await uploadNoInternetReview();

  if (reviewNoInternet != null) {
    Tutoring? tutoring = await getTutoring(reviewNoInternet.tutoringId,
        GetOptions(source: Source.serverAndCache));

    sendEmailReconnectionReview(
        FirebaseAuth.instance.currentUser!.email, tutoring?.title);

    tutoring?.reviews.add(reviewNoInternet.review);
    tutoring?.scores.add(double.parse(reviewNoInternet.score));
    await updateReviews(tutoring!);

    deleteNoInternetReview();
  }
}

void sendEmailReconnectionReview(String? recipient, String? title) async {
  String username = 'moniappuniandes@gmail.com';
  String password = 'vgfkrjnswrxjwtwn';
  final smtpServer = gmail(username, password);

  final message = Message()
    ..from = Address(username)
    ..recipients.add(recipient)
    ..subject = 'Moni regained connection! Your review has been uploaded!'
    ..text =
        'Your review and score have been uploaded for the tutoring $title! Thank you for your feedback and your help in making Moni better!';

  try {
    final sendReport = await send(message, smtpServer);
  } on MailerException {}
}
