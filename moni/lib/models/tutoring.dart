
class Tutoring {
  String? id;
  final String? tutorEmail;
  final String title;
  final String description;
  final String price;
  final String topic;
  final bool inUniversity;
  late int sessionCount;
  final List<String> reviews;
  final List<double> scores;

  Tutoring(
      this.id,
      this.tutorEmail,
      this.title,
      this.description,
      this.price,
      this.topic,
      this.inUniversity,
      this.reviews,
      this.scores,
      this.sessionCount);

  factory Tutoring.fromJson(Map<String?, dynamic> json) {
    try {
      return Tutoring(
          json['id'],
          json['tutorEmail'],
          json["title"],
          json["description"],
          json["price"],
          json['topic'],
          json['inUniversity'],
          json['reviews'] != null
              ? (json['reviews'] as List).map((item) => item as String).toList()
              : [],
          json['scores'] != null
              ? (json['scores'] as List).map((item) => item as double).toList()
              : [],
          json['sessionCount'] ?? 0);
    } catch (e) {
      return Tutoring('a', 'a', 'a', 'a', 'a', 'a', true, [], [], 0);
    }
  }

  Map<String, dynamic> toJson() => {
        'tutorEmail': tutorEmail,
        'title': title,
        'description': description,
        'price': price,
        'topic': topic,
        'inUniversity': inUniversity,
        'reviews': reviews,
        'scores': scores,
        'sessionCount': sessionCount
      };
}
