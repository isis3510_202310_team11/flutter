import 'package:cached_network_image/cached_network_image.dart';
import 'package:moni/models/user.dart';
import 'package:moni/styles.dart';
import 'package:flutter/material.dart';
import 'package:moni/views/profile/profile_form.dart';
import 'package:moni/widgets/navbar.dart';

import '../../controller/auth_controller.dart';
import '../../controller/user_controller.dart';

class Profile extends StatefulWidget {
  Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: NavBar(),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          color: lightGrey,
        ),
        child: ListView(
          children: [
            Row(
              children: [
                Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.fromLTRB(0, 30, 0, 40),
                    child: MaterialButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        'Back',
                        style: rubik16lightBlue,
                      ),
                    )),
                Container(
                  alignment: Alignment.bottomCenter,
                  margin: const EdgeInsets.fromLTRB(50, 30, 0, 40),
                  child: Text(
                    'Profile',
                    style: topViewTitle,
                  ),
                ),
              ],
            ),
            FutureBuilder<User>(
                future: getUserInfo(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Column(children: [
                      CachedNetworkImage(
                        imageUrl: snapshot.data!.profilePicture,
                        imageBuilder: (context, imageProvider) => CircleAvatar(
                          backgroundImage: imageProvider,
                          radius: 80,
                        ),
                        placeholder: (context, url) =>
                            const CircularProgressIndicator(),
                        errorWidget: (context, url, error) =>
                            const Icon(Icons.error),
                      ),
                      Container(
                        margin: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                        child: Text(
                          snapshot.data!.name,
                          style: rubik18darkPurple,
                        ),
                      )
                    ]);
                  }
                  return Container(
                      margin: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                      alignment: Alignment.centerLeft,
                      child: Text('No user found'));
                }),
            ProfileForm(),
            MaterialButton(
                child: Text('Logout'),
                onPressed: () async => logoutHandler(context)),
          ],
        ),
      ),
    );
  }
}
