import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:moni/controller/add_tutoring_controller.dart';
import 'package:moni/styles.dart';

import '../../network/network_connectivity.dart';

class AddTutoringForm extends StatefulWidget {
  const AddTutoringForm({Key? key}) : super(key: key);

  @override
  _AddTutoringFormState createState() => _AddTutoringFormState();
}

class _AddTutoringFormState extends State<AddTutoringForm> {
  late StreamSubscription<dynamic> stream;

  Map _source = {ConnectivityResult.none: false};
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;

  @override
  void initState() {
    super.initState();

    _networkConnectivity.initialise();
    stream = _networkConnectivity.myStream.listen((source) {
      setState(() => _source = source);
    });
  }

  final _formKey = GlobalKey<FormState>();
  bool _validate = false;

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: const EdgeInsets.fromLTRB(25, 0, 0, 20),
              child: Text(
                'What do you teach?',
                style: formTitle,
              ),
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width / 1.08,
              margin: const EdgeInsets.fromLTRB(24, 0, 24, 20),
              padding: const EdgeInsets.fromLTRB(24, 5, 24, 0),
              decoration: BoxDecoration(
                  color: formGrey,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(width: 0.5, color: darkGrey)),
              child: DropdownButtonFormField(
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter tutoring topic';
                  }
                  return null;
                },
                decoration: InputDecoration(
                    border: InputBorder.none, hintText: 'Maths'),
                items: <DropdownMenuItem>[
                  DropdownMenuItem(
                    value: 'Calculus',
                    child: Text('Calculus'),
                  ),
                  DropdownMenuItem(
                    value: 'Physics',
                    child: Text('Physics'),
                  ),
                  DropdownMenuItem(
                    value: 'Dancing',
                    child: Text('Dancing'),
                  ),
                  DropdownMenuItem(
                    value: 'Fitness',
                    child: Text('Fitness'),
                  ),
                ],
                onChanged: (value) {
                  topicField = value;
                },
              )),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: const EdgeInsets.fromLTRB(25, 0, 0, 20),
              child: Text(
                'Tutoring title',
                style: formTitle,
              ),
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width / 1.08,
              margin: const EdgeInsets.fromLTRB(24, 0, 24, 20),
              padding: const EdgeInsets.fromLTRB(24, 5, 24, 0),
              decoration: BoxDecoration(
                  color: formGrey,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(width: 0.5, color: darkGrey)),
              child: TextFormField(
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter tutoring title';
                  }
                  return null;
                },
                controller: titleField,
                decoration: InputDecoration(
                    border: InputBorder.none, hintText: 'Cálculo diferencial'),
              )),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: const EdgeInsets.fromLTRB(25, 0, 0, 20),
              child: Text(
                'Description',
                style: formTitle,
              ),
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width / 1.08,
              margin: const EdgeInsets.fromLTRB(24, 0, 24, 20),
              padding: const EdgeInsets.fromLTRB(24, 5, 24, 0),
              decoration: BoxDecoration(
                  color: formGrey,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(width: 0.5, color: darkGrey)),
              child: TextField(
                controller: descriptionField,
                keyboardType: TextInputType.multiline,
                minLines: 3,
                maxLines: 5,
                decoration: InputDecoration(
                    errorText:
                        _validate ? 'Please enter a class description' : null,
                    border: InputBorder.none,
                    hintText: 'Talk about your class'),
              )),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: const EdgeInsets.fromLTRB(25, 0, 0, 1),
              child: Text(
                'Location',
                style: formTitle,
              ),
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width / 1.08,
              margin: const EdgeInsets.fromLTRB(24, 0, 24, 20),
              padding: const EdgeInsets.fromLTRB(24, 5, 24, 0),
              decoration: BoxDecoration(
                  color: formGrey,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(width: 0.5, color: darkGrey)),
              child: DropdownButtonFormField(
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter tutoring location';
                  }
                  return null;
                },
                decoration: InputDecoration(
                    border: InputBorder.none, hintText: 'University'),
                items: <DropdownMenuItem>[
                  DropdownMenuItem(
                    value: 'University',
                    child: Text('University'),
                  ),
                  DropdownMenuItem(
                    value: 'Outside',
                    child: Text('Outside'),
                  ),
                ],
                onChanged: (value) {
                  locationField = value;
                },
              )),
          Align(
            alignment: Alignment.centerLeft,
            child: Container(
              margin: const EdgeInsets.fromLTRB(25, 0, 0, 20),
              child: Text(
                'Hourly rate',
                style: formTitle,
              ),
            ),
          ),
          Container(
              width: MediaQuery.of(context).size.width / 1.08,
              margin: const EdgeInsets.fromLTRB(24, 0, 24, 20),
              padding: const EdgeInsets.fromLTRB(24, 5, 24, 0),
              decoration: BoxDecoration(
                  color: formGrey,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(width: 0.5, color: darkGrey)),
              child: TextFormField(
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter tutoring hourly rate';
                  }
                  return null;
                },
                controller: priceField,
                decoration:
                    InputDecoration(border: InputBorder.none, hintText: '\$/h'),
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                  CurrencyInputFormatter()
                ],
                keyboardType: TextInputType.number,
              )),
          Align(
              alignment: Alignment.center,
              child: Container(
                margin: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                child: Text(
                  'If you have a problem with your tutor shake your phone',
                  style: TextStyle(fontSize: 14.0),
                ),
              )),
          MaterialButton(
            onPressed: () async => {
              setState(() {
                descriptionField.text.isEmpty
                    ? _validate = true
                    : _validate = false;
              }),
              if (_formKey.currentState!.validate())
                {
                  // If the form is valid, display a snackbar. In the real world,
                  // you'd often call a server or save the information in a database.
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Adding tutoring...')),
                  ),
                  if ((_source.keys.toList()[0] != ConnectivityResult.none &&
                      !_source.values.toList()[0]))
                    {
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                          content: Text(
                              'There is no internet connection, the tutoring can not be created now, please try again when the connection is re-established.'))),
                    },
                  addTutoringHandler(
                    context,
                  )
                }
            },
            child: Container(
                margin: const EdgeInsets.fromLTRB(0, 14, 0, 35),
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                width: MediaQuery.of(context).size.width / 1.08,
                decoration: BoxDecoration(
                    color: darkBlue,
                    borderRadius: BorderRadius.circular(5),
                    boxShadow: shadowInputFields),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'Create',
                      style: rubik16white,
                    ),
                  ],
                )),
          )
        ]));
  }
}
