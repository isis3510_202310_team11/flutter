
class ReviewNoInternet {
  final String tutoringId;
  final String review;
  final String score;

  ReviewNoInternet(this.tutoringId,this.review,this.score);

  Map<String, dynamic> toMap() {
    return {
      'tutoringId': tutoringId,
      'review': review,
      'score': score,
    };
  }

  factory ReviewNoInternet.fromMap(Map<String, dynamic> map) {
    return ReviewNoInternet(
      map['tutoringId'],
      map['review'],
      map['score'],
    );
  }

  Future<String> getTutoringId() async{
    return tutoringId;
  } 

  String getReview(){
    return review;
  }

  String getScore(){
    return score;
  }

}
