import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:moni/controller/register_controller.dart';
import 'package:moni/network/network_connectivity.dart';
import 'package:moni/styles.dart';

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key? key}) : super(key: key);

  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  var visibilityPW = false;
  var visibilityConfirmPW = false;
  final _formKey = GlobalKey<FormState>();

  late StreamSubscription<dynamic> stream;

  Map _source = {ConnectivityResult.none: false};
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;

  @override
  void initState() {
    super.initState();
    _networkConnectivity.initialise();
    stream = _networkConnectivity.myStream.listen((source) {
      setState(() => _source = source);
    });
  }

  @override
  void dispose() {
    stream.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(0, 70, 0, 40),
                child: Text(
                  'Sign Up',
                  style: topViewTitle,
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 1.08,
                margin: const EdgeInsets.fromLTRB(0, 0, 0, 21),
                padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                decoration: BoxDecoration(
                  color: lightGrey,
                  borderRadius: BorderRadius.circular(5),
                  boxShadow: shadowInputFields,
                ),
                child: TextFormField(
                  style: rubik18darkPurple,
                  controller: emailField,
                  cursorColor: darkBlue,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your email';
                    }
                    const pattern =
                        r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'"
                        r'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-'
                        r'\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*'
                        r'[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4]'
                        r'[0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9]'
                        r'[0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\'
                        r'x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])';
                    final regex = RegExp(pattern);

                    return value.isNotEmpty && !regex.hasMatch(value)
                        ? 'Enter a valid email address'
                        : null;
                  },
                  decoration: InputDecoration(
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: darkBlue, width: 3)),
                      labelText: 'Email',
                      labelStyle: rubik18darkPurple,
                      suffixIcon: Icon(
                        Icons.email_outlined,
                        color: softBlue,
                      )),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 1.08,
                margin: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                decoration: BoxDecoration(
                  color: lightGrey,
                  borderRadius: BorderRadius.circular(5),
                  boxShadow: shadowInputFields,
                ),
                child: TextFormField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your name';
                    }
                    return null;
                  },
                  style: rubik18darkPurple,
                  controller: nameField,
                  cursorColor: darkBlue,
                  decoration: InputDecoration(
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: darkBlue, width: 3)),
                      labelText: 'Name',
                      labelStyle: rubik18darkPurple,
                      suffixIcon: Icon(
                        Icons.badge_outlined,
                        color: softBlue,
                      )),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 1.08,
                margin: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                decoration: BoxDecoration(
                  color: lightGrey,
                  borderRadius: BorderRadius.circular(5),
                  boxShadow: shadowInputFields,
                ),
                child: TextFormField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter your password';
                    }
                    return null;
                  },
                  obscureText: !visibilityPW,
                  style: rubik18darkPurple,
                  controller: passwordField,
                  cursorColor: darkBlue,
                  decoration: InputDecoration(
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: darkBlue, width: 3)),
                      labelText: 'Password',
                      labelStyle: rubik18darkPurple,
                      suffixIcon: IconButton(
                        icon: Icon(
                          visibilityPW
                              ? Icons.visibility_outlined
                              : Icons.visibility_off_outlined,
                          color: softBlue,
                        ),
                        onPressed: () => setState(() {
                          visibilityPW = !visibilityPW;
                        }),
                      )),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 1.08,
                margin: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                decoration: BoxDecoration(
                  color: lightGrey,
                  borderRadius: BorderRadius.circular(5),
                  boxShadow: shadowInputFields,
                ),
                child: TextFormField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please confirm your password';
                    }
                    return null;
                  },
                  obscureText: !visibilityConfirmPW,
                  style: rubik18darkPurple,
                  controller: confirmPasswordField,
                  cursorColor: darkBlue,
                  decoration: InputDecoration(
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: darkBlue, width: 3)),
                      labelText: 'Confirm password',
                      labelStyle: rubik18darkPurple,
                      suffixIcon: IconButton(
                        icon: Icon(
                          visibilityConfirmPW
                              ? Icons.visibility_outlined
                              : Icons.visibility_off_outlined,
                          color: softBlue,
                        ),
                        onPressed: () => setState(() {
                          visibilityConfirmPW = !visibilityConfirmPW;
                        }),
                      )),
                ),
              ),
              Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    margin: const EdgeInsets.fromLTRB(25, 30, 0, 20),
                    child: Text(
                      'Please select your two preferred interests',
                      style: TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  )),
              Container(
                width: MediaQuery.of(context).size.width / 1.08,
                margin: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                decoration: BoxDecoration(
                    color: lightGrey,
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(width: 0.5, color: darkGrey)),
                child: DropdownButtonFormField(
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please choose your first interest';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                      border: InputBorder.none, hintText: 'First interest'),
                  items: <DropdownMenuItem>[
                    DropdownMenuItem(
                      value: 'Calculus',
                      child: Text('Calculus'),
                    ),
                    DropdownMenuItem(
                      value: 'Physics',
                      child: Text('Physics'),
                    ),
                    DropdownMenuItem(
                      value: 'Dancing',
                      child: Text('Dancing'),
                    ),
                    DropdownMenuItem(
                      value: 'Fitness',
                      child: Text('Fitness'),
                    ),
                  ],
                  onChanged: (value) {
                    interest1 = value;
                  },
                  // controller: topicField
                ),
              ),
              Container(
                  width: MediaQuery.of(context).size.width / 1.08,
                  margin: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                  padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                  decoration: BoxDecoration(
                      color: lightGrey,
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(width: 0.5, color: darkGrey)),
                  child: DropdownButtonFormField(
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please choose your second interest';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        border: InputBorder.none, hintText: 'Second interest'),
                    items: <DropdownMenuItem>[
                      DropdownMenuItem(
                        value: 'Calculus',
                        child: Text('Calculus'),
                      ),
                      DropdownMenuItem(
                        value: 'Physics',
                        child: Text('Physics'),
                      ),
                      DropdownMenuItem(
                        value: 'Dancing',
                        child: Text('Dancing'),
                      ),
                      DropdownMenuItem(
                        value: 'Fitness',
                        child: Text('Fitness'),
                      ),
                    ],
                    onChanged: (value) {
                      interest2 = value;
                    },
                  )),
              MaterialButton(
                onPressed: () async => {
                  if (_formKey.currentState!.validate())
                    {
                      // If the form is valid, display a snackbar. In the real world,
                      // you'd often call a server or save the information in a database.
                      if (passwordField.text != confirmPasswordField.text)
                        {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text('Passwords do not match')),
                          ),
                        },
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text('Signing up...')),
                      ),
                      registerHandler(context),
                    }
                },
                child: (_source.keys.toList()[0] != ConnectivityResult.none &&
                        !_source.values.toList()[0])
                    ? MaterialButton(
                        onPressed: () => {
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                    content: Text('No internet connection')),
                              ),
                            },
                        child: Container(
                            margin: const EdgeInsets.fromLTRB(0, 150, 0, 20),
                            padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                            width: MediaQuery.of(context).size.width / 1.08,
                            decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.circular(5),
                                boxShadow: shadowInputFields),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  'Can\'t connect to the internet',
                                  style: rubik16white,
                                ),
                              ],
                            )))
                    : Container(
                        margin: const EdgeInsets.fromLTRB(0, 14, 0, 35),
                        padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                        width: MediaQuery.of(context).size.width / 1.08,
                        decoration: BoxDecoration(
                            color: darkBlue,
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: shadowInputFields),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Sign Up',
                              style: rubik16white,
                            ),
                          ],
                        )),
              ),
            ],
          ),
        ));
  }
}
