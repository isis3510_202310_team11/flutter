import 'package:cloud_firestore/cloud_firestore.dart';

class Session {
  final String? tutoringId;
  final String? tutorEmail;
  final String? clientEmail;
  final DateTime meetingDate;
  final String? place;

  Session(this.tutoringId, this.tutorEmail, this.clientEmail, this.meetingDate,
      this.place);

  factory Session.fromJson(Map<String, dynamic> json) {
    return Session(json["tutoringId"], json["tutorEmail"], json['clientEmail'],
        (json['meetingDate'] as Timestamp).toDate(), json['place']);
  }

  Map<String, dynamic> toJson() => {
        'tutoringId': tutoringId,
        'tutorEmail': tutorEmail,
        'clientEmail': clientEmail,
        'meetingDate': meetingDate,
        'place': place,
      };

  Map<String, dynamic> toMap() {
    return {
      'tutoringId': tutoringId,
      'tutorEmail': tutorEmail,
      'clientEmail': clientEmail,
      'meetingDate': meetingDate,
      'place': place,
    };
  }

  factory Session.fromMap(Map<String, dynamic> map) {
    return Session(
      map['tutoringId'],
      map['tutorEmail'],
      map['clientEmail'],
      map['meetingDate'],
      map['place'],
    );
  }
}
