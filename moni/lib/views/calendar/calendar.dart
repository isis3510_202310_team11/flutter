import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:moni/models/session.dart';
import 'package:moni/repository/session_repository.dart';
import 'package:moni/styles.dart';
import 'package:moni/widgets/navbar.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';

import '../../controller/calendar_controller.dart';
import '../../network/network_connectivity.dart';
import 'package:url_launcher/url_launcher.dart';

class Calendar extends StatefulWidget {
  const Calendar({Key? key}) : super(key: key);

  @override
  _CalendarState createState() => _CalendarState();
}

class _CalendarState extends State<Calendar> {
  ValueNotifier<List<Session>>? _selectedSessions = ValueNotifier([]);
  DateTime _focusedDay = DateTime.now();
  DateTime? _selectedDay;
  late StreamSubscription<dynamic> stream;

  Map _source = {ConnectivityResult.none: false};
  final NetworkConnectivity _networkConnectivity = NetworkConnectivity.instance;

  @override
  void initState() {
    super.initState();

    _selectedDay = _focusedDay;

    _networkConnectivity.initialise();
    stream = _networkConnectivity.myStream.listen((source) {
      setState(() => _source = source);
    });
  }

  @override
  void dispose() {
    stream.cancel();
    super.dispose();
  }

  CalendarFormat _calendarFormat = CalendarFormat.month;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: NavBar(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              FutureBuilder<Map<String, List<Session>>>(
                future: getSessionsForUser(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    _selectedSessions?.value = snapshot
                            .data![DateFormat.yMd().format(_selectedDay!)] ??
                        [];

                    return Column(
                      children: [
                        TableCalendar(
                          eventLoader: ((day) {
                            return snapshot
                                    .data![DateFormat.yMd().format(day)] ??
                                [];
                          }),
                          headerStyle: HeaderStyle(
                            decoration: BoxDecoration(color: lightBlue),
                            headerPadding: EdgeInsets.fromLTRB(0, 75, 0, 25),
                            titleCentered: true,
                            formatButtonVisible: false,
                            titleTextStyle: topViewTitleWhite,
                            titleTextFormatter: (date, locale) =>
                                DateFormat.MMMM(locale).format(date),
                          ),
                          calendarStyle: CalendarStyle(
                              selectedTextStyle: rubik14black,
                              selectedDecoration: BoxDecoration(
                                  border:
                                      Border.all(color: lightBlue, width: 5.0),
                                  shape: BoxShape.circle),
                              todayDecoration: (BoxDecoration(
                                  color: lightBlue, shape: BoxShape.circle))),
                          firstDay: DateTime.utc(2010, 10, 16),
                          lastDay: DateTime.utc(2030, 3, 14),
                          focusedDay: _focusedDay,
                          calendarFormat: _calendarFormat,
                          selectedDayPredicate: (day) {
                            // Use `selectedDayPredicate` to determine which day is currently selected.
                            // If this returns true, then `day` will be marked as selected.

                            // Using `isSameDay` is recommended to disregard
                            // the time-part of compared DateTime objects.
                            return isSameDay(_selectedDay, day);
                          },
                          onDaySelected: (selectedDay, focusedDay) {
                            if (!isSameDay(_selectedDay, selectedDay)) {
                              // Call `setState()` when updating the selected day
                              setState(() {
                                _selectedDay = selectedDay;
                                _focusedDay = focusedDay;
                              });

                              _selectedSessions?.value = snapshot.data![
                                      DateFormat.yMd().format(selectedDay)] ??
                                  [];
                            }
                          },
                          onPageChanged: (focusedDay) {
                            // No need to call `setState()` here
                            _focusedDay = focusedDay;
                          },
                        ),
                        _selectedSessions!.value.isNotEmpty
                            ? SizedBox(
                                height: MediaQuery.of(context).size.height / 4,
                                child: ListView(
                                  shrinkWrap: true,
                                  // This next line does the trick.
                                  children: <Widget>[
                                    ...?_selectedSessions?.value
                                        .map((e) => Container(
                                            padding: EdgeInsets.fromLTRB(
                                                40, 10, 40, 20),
                                            decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        width: 0.4,
                                                        color: Colors.grey))),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Container(
                                                  decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      color: yellow),
                                                  height: 20,
                                                  width: 20,
                                                ),
                                                Text(
                                                    e.place ??
                                                        'No place specified',
                                                    style: rubik18darkPurple),
                                                Text(
                                                    DateFormat.Hm()
                                                        .format(e.meetingDate),
                                                    style: rubik18darkPurple)
                                              ],
                                            )))
                                        .toList()
                                  ],
                                ))
                            : Container(
                                margin: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                                child: Text('No sessions for today',
                                    style: rubik18darkPurple),
                              ),
                        Container(
                          margin: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                          child: Column(children: [
                            nextSession(snapshot.data!)?.tutorEmail! != null
                                ? Container(
                                    margin: EdgeInsets.fromLTRB(30, 0, 30, 20),
                                    child: InkWell(
                                      onTap: () => {
                                        launchUrl(Uri(
                                            scheme: 'mailto',
                                            path:
                                                '${nextSession(snapshot.data!)?.tutorEmail!}',
                                            query:
                                                'subject=Regarding%20your%20session%20with%20${nextSession(snapshot.data!)?.clientEmail!}'))
                                      },
                                      child: Text(
                                        'Click here to open a contact ${nextSession(snapshot.data!)?.tutorEmail!} regarding your session',
                                        style: TextStyle(),
                                      ),
                                    ),
                                  )
                                : Container(),
                            Container(
                              margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                              child: Text(
                                  '${_selectedSessions?.value.length} sessions for today',
                                  style: rubik18darkPurple),
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                              child: Text(
                                  '${sessionsBeforeToday(snapshot.data!).length} past sessions',
                                  style: rubik18darkPurple),
                            ),
                            Container(
                              margin: const EdgeInsets.fromLTRB(0, 0, 0, 50),
                              child: Text(
                                  '${sessionsAfterToday(snapshot.data!).length} upcoming sessions',
                                  style: rubik18darkPurple),
                            ),
                          ]),
                        )
                      ],
                    );
                  }
                  return Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 1.2,
                      margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                      alignment: Alignment.center,
                      child: CircularProgressIndicator());
                },
              ),
            ],
          ),
        ));
  }
}
