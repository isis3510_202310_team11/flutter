import 'package:moni/dao/database_helper_review.dart';

import '../models/reviewNoInternet.dart';

class ReviewDao {
  final dbHelper = DatabaseHelperReview.instance;

  Future<int> insert(ReviewNoInternet review) async {
    final db = await dbHelper.database;
    return await db.insert(DatabaseHelperReview.table, review.toMap());
  }

  Future<List<ReviewNoInternet>> queryAll() async {
    final db = await dbHelper.database;
    List<Map<String, dynamic>> maps =
        await db.query(DatabaseHelperReview.table);
    return maps.map((map) => ReviewNoInternet.fromMap(map)).toList();
  }

  Future<int> deleteAll() async {
    final db = await dbHelper.database;
    return await db.delete(DatabaseHelperReview.table);
  }
}
