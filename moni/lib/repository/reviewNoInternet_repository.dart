import '../dao/reviews_dao.dart';
import '../models/reviewNoInternet.dart';
import 'dart:async';

final reviewDao = ReviewDao(); // Instantiate your DAO class

Future<ReviewNoInternet?> uploadNoInternetReview() async {
  List<ReviewNoInternet> result = await reviewDao.queryAll();
  if (result.isNotEmpty) {
    ReviewNoInternet reviewNIToUpload = result.removeLast();
    return reviewNIToUpload;
  } else {
    return null;
  }
}

Future<ReviewNoInternet?> deleteNoInternetReview() async {
  await reviewDao.deleteAll();
  return null;
}
