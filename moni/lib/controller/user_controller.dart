import 'package:firebase_auth/firebase_auth.dart' as FirebaseAuth;

import 'package:moni/models/user.dart';

import '../repository/user_repository.dart';

Future<User> getUserInfo() async {
  User? user =
      await getUser(FirebaseAuth.FirebaseAuth.instance.currentUser?.email);

  if (user == null) return User('', '', '', '', '');

  return user;
}
