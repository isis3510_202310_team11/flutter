import 'package:moni/controller/navigation_controller.dart';
import 'package:moni/models/tutoring.dart';
import 'package:moni/styles.dart';
import 'package:flutter/material.dart';

import '../../controller/image_controller.dart';

class CardDescubreFechas extends StatefulWidget {
  final Tutoring tutoring;

  const CardDescubreFechas({Key? key, required this.tutoring})
      : super(key: key);

  @override
  _CardDescubreFechasState createState() => _CardDescubreFechasState();
}

class _CardDescubreFechasState extends State<CardDescubreFechas> {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
        child: Column(
          children: [
            MaterialButton(
              padding: EdgeInsets.zero,
              onPressed: () {
                scheduleTap(context, widget.tutoring);
              },
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.asset(
                  getImage(widget.tutoring.topic),
                  height: MediaQuery.of(context).size.height * 0.13,
                  width: MediaQuery.of(context).size.height * 0.13,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
                width: double.infinity,
                margin: EdgeInsets.fromLTRB(2, 15, 0, 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.tutoring.title,
                      style: rubik14black,
                    ),
                    Container(
                        margin: EdgeInsets.fromLTRB(2, 10, 0, 0),
                        child: Text(
                          widget.tutoring.price,
                          style: rubik14blackBold,
                        )),
                  ],
                )),
          ],
        ));
  }
}
