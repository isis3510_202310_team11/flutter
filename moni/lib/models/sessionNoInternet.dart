
class SessionNoInternet {
  final String tutoringId;
  final String meetingDate;
  final String place;

  SessionNoInternet(this.tutoringId,this.meetingDate,this.place);

  Map<String, dynamic> toMap() {
    return {
      'tutoringId': tutoringId,
      'meetingDate': meetingDate,
      'place': place,
    };
  }

  factory SessionNoInternet.fromMap(Map<String, dynamic> map) {
    return SessionNoInternet(
      map['tutoringId'],
      map['meetingDate'],
      map['place'],
    );
  }

  Future<String> getTutoringId() async{
    return tutoringId;
  } 

  String getMeetingDate(){
    return meetingDate;
  }

  String getPlace(){
    return place;
  }

}
